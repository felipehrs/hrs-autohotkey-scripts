ToggleSuperHidden(){
	Global
	RegRead, SuperHiddenFiles_Status, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, ShowSuperHidden
	If HiddenFiles_Status = 2
		RegWrite, REG_DWORD, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, ShowSuperHidden, 1
	Else
		RegWrite, REG_DWORD, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, ShowSuperHidden, 2
}

ToggleHidden(){
	Global
	RegRead, HiddenFiles_Status, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, Hidden
	If HiddenFiles_Status = 2
		RegWrite, REG_DWORD, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, Hidden, 1
	Else
		RegWrite, REG_DWORD, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, Hidden, 2
}

ShowExtensions(){
	Global
	RegRead, HiddenFiles_Status, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, HideFileExt
	If HiddenFiles_Status = 1
		RegWrite, REG_DWORD, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, HideFileExt, 0
	Else
		RegWrite, REG_DWORD, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced, HideFileExt, 1
}

RefreshWindows(){
	Global
	;WinGetClass, eh_Class,A
	;If (eh_Class = "#32770" OR A_OSVersion = "WIN_VISTA")
	send, {F5}
	;Else PostMessage, 0x111, 28931,,, A
}

;Hidden Files (WINDOWS KEY + H)
#h::
ToggleHidden()
RefreshWindows()
return

;Super Hidden Files ((WINDOWS + SHIFT + H)
#+h::
ToggleHidden()
ToggleSuperHidden()
RefreshWindows()
return

;TOGGLES FILE EXTENSIONS (WINDOWS + Y)
#y::
ShowExtensions()
RefreshWindows()
Return
