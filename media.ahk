#F5::
Send {Volume_Down}
return

#F6::
Send {Volume_Up}
return

#F7::
Send {Volume_Mute}
return

#F1::
Send {Media_Prev}
return

#F2::
Send {Media_Next}
return

#F3::
Send {Media_Play_Pause}
return

#F4::
Send {Media_Stop}
return