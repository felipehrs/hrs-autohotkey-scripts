# My AutoHotKeys scripts

| Hotkey                     |     Command
|----------------------------|---
| Windows Key + H            |     Toggle "Show hidden files"
| Windows Key + Shift + H    |     Toggle "Show system hidden files"
| Windows Key + Y            |     Toggle "Show file extensions"
| Ctrl + Alt + T             |     Open ConEmu64
| Ctrl + Windows Key + T     |     Open my TCC document
| Ctrl + Windows + A         |     Open my google spreadsheet with my work schedules
| Windows Key + F1           |     Media Previous
| Windows Key + F2           |     Media Previous
| Windows Key + F3           |     Media Play/Pause
| Windows Key + F4           |     Media Stop
| Windows Key + F5           |     Media Volume Down
| Windows Key + F6           |     Media Volume Up
| Windows Key + F7           |     Media Volume Mute