#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; + - Shift
; ^ - Ctrl
; ! - Alt
; # - Windows Key

;#####################
;#####  HOTKEYS  #####
;#####################

#Include, %A_ScriptDir%\explorerUtils.ahk

#Include, %A_ScriptDir%\console.ahk

#Include, %A_ScriptDir%\tcc.ahk

#Include, %A_ScriptDir%\apontamentos.ahk

#Include, %A_ScriptDir%\media.ahk